# aws-notes

Notes on AWS

## Identify your user id, account and username

aws sts get-caller-identity

## Identify your region 

aws configure get region

## How to work with multiple accounts

Use profiles in the configuration files in .aws folder

## Configure new connection profile if you need to:

aws configure --profile yourprofile

## Switch to the appropriate profile:

### windows:

set AWS_PROFILE=yourotherprofile

### bash:

AWS_PROFILE=yourotherprofile

## Setup MFA

aws sts get-session-token --serial-number arn:aws:iam::<account>:mfa/<username> --token-code <mfa code>

## Package a lambda

sam package --template-file template.yml --output-template-file additional_cloud_formation_templates/lambda_template_package.yml --s3-bucket yourlambda-code-bucket

#### Deploy Lambda

Many parameter overrides in this one may not be needed in your case...

sam deploy --template-file additional_cloud_formation_templates/lambda_template_package.yml --parameter-overrides Env=dev GroupAndTagReadOnly="arn:aws:iam::aws:policy/ResourceGroupsandTagEditorReadOnlyAccess" PolicyGroup="ausergroupname" PolicyName="apolicyname" S3AccessReadOnly="arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess" VpcAccessExecutionRole="arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole" VpcConfig="s3://somelocation/DEV/VpcConfig.yml" --stack-name astackname --capabilities CAPABILITY_IAM

#### navigating documentation for cloudformation templates

Always google the "Type" of elements not their name e.g.

SftpUserBucket: Type: AWS::S3::Bucket

Don't google "SftpUserBucket", google "AWS::S3::Bucket"

#### "Outputs:" Section

This is only useful if you reference template A from inside of template B. If template A is not going to be referenced from anywhere just ommit this section.
Confirming changes

#### Review changes

Often even a refresh of a certain page will not present newly applied changes (!!!) try using incognito mode or navigating to the page differently.
